<?php
// $Id$
/**
 * @file
 * OAuth delegation callbacks for the facebook module.
 */

/**
 * Menu callback; Starts a Facebook authentication.
 */
function facebook_oauth_authenticate(DrupalOAuthConsumer $oauth) {
  $_SESSION['facebook']['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
  $callback = 'oauth/' . 'facebook' . '/oauth_callback';
  $options['query'] = array(
    'state' => $_SESSION['facebook']['state'],
    'client_id' => variable_get('facebook_client_id'),
    'redirect_uri' => url($callback, array('absolute' => TRUE))
  );

  header("Location:" . url('http://www.facebook.com/dialog/oauth', $options));
}

/**
 * Implements hook_oauth_callback; Process an OAuth response from Facebook.
 */
function facebook_oauth_callback(DrupalOAuthConsumer $oauth) {
  $code = $_REQUEST["code"];

  $debug = TRUE;
}